﻿using Microsoft.AspNetCore.Http;
using SevenPeaks.Core.Logging.DataAccess;
using SevenPeaks.Core.Logging.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SevenPeaks.Core.Logging.Business
{
    public interface IWeatherForecastManager
    {
        IList<WeatherForecast> GetWeathers();
    }

    public class WeatherForecastManager : IWeatherForecastManager
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWeatherForecastRepository _weatherForecastRepository;

        public WeatherForecastManager(IHttpContextAccessor httpContextAccessor,
                                      IWeatherForecastRepository weatherForecastRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _weatherForecastRepository = weatherForecastRepository;
        }

        private static readonly string[] Summaries = new[]
{
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public IList<WeatherForecast> GetWeathers()
        {
            var weathers = _weatherForecastRepository.GetWeathers();
            var rng = new Random();

            for (int i = 0; i < 5; i++)
            {
                weathers.Add(new WeatherForecast
                {
                    CustomHeader = $"Business access custom header:{ _httpContextAccessor.HttpContext.Request.Headers["X-Custom-Header"] }",
                    Date = DateTime.UtcNow.AddDays(i),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                });
            }

            return weathers;
        }
    }
}
