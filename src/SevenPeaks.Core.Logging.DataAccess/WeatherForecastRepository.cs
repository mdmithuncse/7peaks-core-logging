﻿using Microsoft.AspNetCore.Http;
using SevenPeaks.Core.Logging.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SevenPeaks.Core.Logging.DataAccess
{
    public interface IWeatherForecastRepository
    {
        IList<WeatherForecast> GetWeathers();
    }

    public class WeatherForecastRepository : IWeatherForecastRepository
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WeatherForecastRepository(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public IList<WeatherForecast> GetWeathers()
        {
            var rng = new Random();
            List<WeatherForecast> weathers = new List<WeatherForecast>();

            for (int i = 0; i < 5; i++)
            {
                weathers.Add(new WeatherForecast
                {
                    CustomHeader = $"Data access custom header:{ _httpContextAccessor.HttpContext.Request.Headers["X-Custom-Header"] }",
                    Date = DateTime.UtcNow.AddDays(i),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                });
            }

            return weathers;
        }
    }
}
