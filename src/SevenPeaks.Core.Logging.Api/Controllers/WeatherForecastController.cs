﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SevenPeaks.Core.Logging.Business;
using SevenPeaks.Core.Logging.Models;
using System.Collections.Generic;

namespace SevenPeaks.Core.Logging.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IWeatherForecastManager _weatherForecastManager;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, 
                                         IWeatherForecastManager weatherForecastManager)
        {
            _logger = logger;
            _weatherForecastManager = weatherForecastManager;
        }

        [HttpGet]
        public IList<WeatherForecast> Get()
        {
            return _weatherForecastManager.GetWeathers();
        }

        [HttpGet]
        [Route("/CustomHeader")]
        public string GetHeader()
        {
            return HttpContext.Request.Headers["X-Custom-Header"];
        }

        [HttpGet]
        [Route("/CustomHeaders")]
        public IHeaderDictionary GetAllHeaders()
        {
            return HttpContext.Request.Headers;
        }
    }
}
